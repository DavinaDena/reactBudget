import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch } from "react-router-dom";

import { Home } from "./pages/Home";



export default function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div>
      
        <Switch>
          <Route path="/" exact>
            <Home/>
          
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
