import { useState } from "react"


const initialState = {
    date: '',
    category: '',
    money: ''
}

//FORM BUDGET BOOTSTRAP
export function InsertBudget({ onBudSubmit, operation=initialState}) {

    const [budget, setBudget] = useState(operation);

    const handleChange=event=> {
        setBudget({
            ...budget,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onBudSubmit(budget);
    }

    return (
        <div>
            <div >
                <form onSubmit={handleSubmit} className="justify-content-end">
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput1" className="form-label">Category:</label>
                        <input type="text" required className="form-control" placeholder="beer" 
                        name="category" onChange={handleChange} value={budget.category} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput2" className="form-label">Money:</label>
                        <input type="text" className="form-control" 
                        name="money" onChange={handleChange} value={budget.money} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput3" className="form-label">Date:</label>
                        <input type="date" required className="form-control" placeholder="12-30-2021"
                         name="date" onChange={handleChange} value={budget.date} />
                    </div>
                    <div className="btn-group" role="group" aria-label="Basic example">
                        <button className="btn btn-changeColor">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    )
}
