



export function ShowBudget({ budget, onDelete}) {

    
    return (
        <>

            <tr>
                <td colSpan="1"> {new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(budget.money)} </td>
                <td colSpan="1"> {budget.category} </td>
                <td colSpan="1">{new Intl.DateTimeFormat('en-GB',
                    {
                        year: 'numeric',
                        month: 'long',
                        day: '2-digit'
                    }
                ).format(new Date(budget.date))} </td>
                <td colSpan="1"> <button className="btn btn-danger" onClick={() => onDelete(budget.id)}>X</button></td>
                <td colSpan="5"></td>
            </tr>
            
        </>
    )
}