import { useEffect, useState } from "react";

export function TotalBudget({ budget }) {

    const [balance, setBalance] = useState(null)

    useEffect(() => {
        function bankBalance() {
            let balance = 0;
            for (let item of budget) {
                
                balance += Number(item.money);
            }
            setBalance(balance)
        }
        bankBalance()
    }, [budget])

    return (


        <p>{new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(balance)}</p>

    )
}