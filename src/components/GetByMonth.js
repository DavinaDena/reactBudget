


export function GetByMonth({ selectMonth, reset }) {

    /**
     * function to select by month
     * @param {month} month 
     */
    function handleSelect(month) {
        selectMonth(month)
    }

    /**
     * to reset the search(gives everything)
     */
    function handleReset() {
        reset()
    }

    /**
     * 
     * @param {string} string 
     * @returns the string with the first letter on capital
     */
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getMonthsForLocale(locale) {
        let format = new Intl.DateTimeFormat(locale, { month: 'long' })
        let months = []
        for (let month = 0; month < 12; month++) {
            let testDate = new Date(Date.UTC(2000, month, 1, 0, 0, 0));
            months.push(capitalizeFirstLetter(format.format(testDate)))
        }
        return months;
    }

    const months = getMonthsForLocale('en-EN')

    return (
        <div>
    
            <div className="dropdown">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                    Select Month
                </button>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                    <li className="dropdown-item active" onClick={handleReset}>EveryMonth</li>
                    {months.map((month, index)=> <li key={index} className="dropdown-item" onClick={()=>handleSelect(index+1)}> {month}</li>)}
                </ul>
            </div>
        </div>
    )
}


