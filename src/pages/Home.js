
import { useEffect, useState } from "react"
import { ShowBudget } from "../components/ShowBudget"
import { InsertBudget } from "../components/InsertBudget"
import { BudgetService } from "../shared/BudgetService";
import { SearchBudget } from "../components/SearchBudget";
import { GetByMonth } from "../components/GetByMonth";
import { TotalBudget } from "../components/TotalBudget";



export function Home() {

    const [budget, setBudget] = useState([]);
    useEffect(() => {
        getBudget();
        
    }, []);

    /**
     * function to get all budget
     */
    async function getBudget() {
        const answer = await BudgetService.getAll();
        setBudget(answer.data)
    }

    /**
     * fuction to add budget
     * @param {budget} bud 
     */
    async function add(bud) {
        const answer = await BudgetService.addBudget(bud);
        setBudget([
            ...budget,
            answer.data
        ])
    }

    /**
     * function to delete budget
     * @param {id} id 
     */
    async function deleteBud(id) {
        await BudgetService.deleteBudget(id)
        setBudget(
            budget.filter(item => item.id !== id)
        )
    }

    /**
     * function to get balance by month
     * @param {month} month 
     */
    async function getByMonth(month) {
        const answer = await BudgetService.getByMonth(month)
        setBudget(answer.data)
    }

    /**
     * function to get budget/category/date that matches
     * @param {anything} search 
     */
    const handleSearch = async (search) => {
        const data = await BudgetService.search(search)
        setBudget(data)
    }


 

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-9">
                        <SearchBudget onSearch={handleSearch} />
                    </div>
                    <div className="col-md-2 d-flex justify-content-end m-2">
                        <GetByMonth reset={getBudget} selectMonth={getByMonth} />
                    </div>
                    <div className="row">

                    
                    <div className="col-md-6">
                        <h1>Welcome to your bank page</h1>
                        <table className="table table-dark table-striped">
                            <thead>
                                <tr>
                                    <th >Money</th>
                                    <th >Category</th>
                                    <th >Date</th>
                                    <th>Delete</th>
                                    <th>Total : <TotalBudget budget={budget}/></th>
                                </tr>
                            </thead>
                            <tbody>
                                {/* TABLE OF BUDGET*/}
                                {budget.map(item =>
                                    <ShowBudget key={item.id}
                                        budget={item}
                                        onDelete={deleteBud}
                                    />
                                )}
                            </tbody>
                        </table>
                    </div>

                    <div className="offset-md-1 col-md-5">
                        {/* FORM BUDGET */}
                        <h2>Submit your savings/spendings</h2>
                        <InsertBudget onBudSubmit={add} />
                    </div>
                    </div>
                </div>
            </div>
        </>
    )
}