import axios from "axios";



export class BudgetService {

    /**
     * 
     * @param {anything} term 
     * @returns the matching properties
     */
    static async search(term) {
        const answer = await axios.get('https://budgetappdaniela.herokuapp.com/api/budget?search=' + term)
        return answer.data
    }

    /**
     * 
     * @returns all budget
     */
    static async getAll() {
        return axios.get('https://budgetappdaniela.herokuapp.com/api/budget')
    }

    /**
     * 
     * @param {budget} budget 
     * @returns the new budget
     */
    static async addBudget(budget) {
        return axios.post('https://budgetappdaniela.herokuapp.com/api/budget', budget)
    }

    /**
     * deletes a statement from the table
     * @param {id} id 
     */
    static async deleteBudget(id) {
        axios.delete('https://budgetappdaniela.herokuapp.com/api/budget/' + id)
    }

    // static async update(){
        
    // }

    /**
     * 
     * @param {month} month 
     * @returns the budget from that month
     */
    static async getByMonth(month){
        return axios.get('https://budgetappdaniela.herokuapp.com/api/budget/month/' + month)
    }

}