# Budget Project
##### Back(Api with express, node and SQL) and Front (with react and bootstrap)

For this project I had to create an application back and front end.

### 1. Back-End description

For the back end, I choose to work with only one table on SQL. This table contains what I found to be most important and that is an ID, Date, Category and Money. I also inserted some values to later manipulate.

Then I created my workspace, an organised folder to start with the API and installed jest, babel, express, nodemon, dotenv and cors. 

This done, it was time to start working:
1. Right away I created the file .env so I could connect my workspace with my table.
2. On the file App.js, I created the const port and imported "dotenv-flow".
3. Created the file Server.js and const server.
4. Now it was time for the entity which I made with a class.
5. Created the file Connection.js on the Repository and imported a Pool and also connected with my .env.
6. Still on the Repository, I started to create my functions with a class to make the requests to my table, the functions I created are:
-To get all budget
-To search on the budget (with a param, can search by category, amount of money or date -> but written with numbers)
-To add a statement into our table
-To delete a statement
-To update a statement
-To find by ID
-To find By month
-To find by amount and category but ended up not using them
7.Now I created the controller and then tried it on the browser and with ThunderClient on VisualStudioCode

After testing everything I could confortably pass to the front-end
(Althought later I found that some functions on my repository weren't as good as I thought and also my get/post/etc and my /:id/etc...)

### 2. Front-End description

The front end I did it with react and with Bootstrap.
Again, I created another workplace with npx create-react-app and then installed reacter-router-dom, axios and material UI.

After creating the folders pages, components ans shared, once again, I could start to work so:
1. On App.js I made a Route to Home
2. Created the file Home.js on the folder pages
3. Created the file Service on Shared. Here I imported axios and made a class (find it more easier to organise like that) and created async functions that can connect to the API and then to my table in SQL
4. Now is when I started to create the function Home. Here I used useState and useEffect and also made some functions to use the class Service and retreive the data.
5. To have something populating the function Home, I created some components like
-Dropdown to get the budget by month
-Form to insert new statements
-An input to serve as a search bar
-A component to show my statements

On the front I decided to show the statements with a table, a form and a button from Bootstrap. I only used the material UI for the search bar.
I also decided not to import bootstrap but just to copy the links into the index page.

On the page Home I did a function to calculate the total althought everytime I add a statement, the total doesn't actualize right away. I think I made some mistakes in my code structure and architecture and that's why also it's hard for me to solve the problem since when I inserted the total on useEffect, it would actualize the total but I couldn't show the budget per month since that also actualizes the
